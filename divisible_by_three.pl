#! /usr/bin/perl 

# Copyright 2018 Ansgar Gruber
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#   http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# usage: perl divisible_by_three.pl your_filename
# output file: your_filename_divisible_by_three
# Runs with Perl v5.18.2 on Darwin (and possibly with other Perl installations)

use warnings;
use strict;

# name of fasta file
my $infile_fn;
if ( scalar(@ARGV) == 1 ) {
    $infile_fn = shift(@ARGV);
}
else {
    print(
        "Please type the name of the file that shall be checked for divisibility by three.\n"
    );
    $infile_fn = <stdin>;
}
chomp($infile_fn);
unwhite($infile_fn);    # remove any remaining whitespace

# load sequences from file, check and correct (by subroutine mod)
open( my $fasta_fh, "<", $infile_fn )
  or die( "divisible_by_three\.pl is unable to open " . $infile_fn . ", please check.\n" );
my $file_name = "sequence names";    # names in $fasta_fh file
my $file_seq  = "yyy";                 # sequences in $fasta_fh file
my %fasta =
  ();    # hash with sequences from file, names as keys and sequences as values
my %fasta_order = ()
  ; # hash with sequence names from file, number as key and sequence name as value
my $n = 0;    # number of sequences
my $d = 0;    # number of deleted sequences

while ( my $inputline = <$fasta_fh> ) {
    chomp($inputline);
    unwhite($inputline);      # remove any remaining whitespace
    my $firstCh = substr( $inputline, 0, 1 );
    unless ( $firstCh eq ";" ) {    # ignore comment lines
        if ( $firstCh eq ">" ) {    # new sequence identifier
            if (mod($file_seq)) {   # sequence is divisible by three or was successfully truncated
                $fasta{$file_name} = mod($file_seq);
                $fasta_order{$n}   = $file_name;
                $file_seq          = ();
                $file_name = substr( $inputline, 1 );
                $n++;
            }
            else {
                $file_seq          = ();
                $file_name = substr( $inputline, 1 );
                $d++; # deleted sequence
            }
        }
        else {
            $file_seq .= $inputline;
        }
    }
}
if (mod($file_seq)) {   # sequence is divisible by three or was successfully truncated
    $fasta{$file_name} = mod($file_seq);
    $fasta_order{$n}   = $file_name;
    $file_seq          = ();
}
else {
    $file_seq          = ();
    $n--; # adjust index (was switched to next divisible seq already, which eventually did not appear.)
    $d++; # deleted sequence
}
close($fasta_fh);
delete $fasta{"sequence names"};
delete $fasta_order{0}
  ; # get rid of initial pair, which is needed to avoid undefined hashkey at first iteration of while loop



# print checked sequences into new file
open( my $outfile_fh, ">",
    $infile_fn . "._divisible_by_three" )    # FASTA with sequences truncated to divisible by three lengths
  or die( "divisible_by_three\.pl is unable to open "
      . $infile_fn . "._divisible_by_three"
      . ", please check.\n" );
my $np = 1;                 # index of sequence names
while ( $np <= $n ) {       # continue until all sequences are printed
    print $outfile_fh (
            ">" . $fasta_order{$np} . "\n" . $fasta{ $fasta_order{$np} } );
    $np++;
}
close($outfile_fh);

# say goodbye
print(  "\n"
      . $n . " of " . ($n+$d)
      . " sequences from "
      . $infile_fn
      . " are divisible by three or have been truncated and re-written to "
      . $infile_fn . "._divisible_by_three\n"
      . $d
      . " sequences could not be truncated due to lack of start or stop codons"
      . ".\n" );
print(
"\nThank you for using divisible_by_three\.pl, please contact ansgar.gruber\@paru.cas.cz \nwith any question concerning the program :-\)\n"
);

sub mod
{ # removes gaps and whitespace, truncates sequences to the highest possible
  # length that is divisible by three, converts to upper case, inserts
  # linebreaks every 60 residues 
    unless ( scalar(@_) == 1 ) {
        die(
"\nSubroutine mod needs exactly one element of \@\_, program died, please check.\n"
        );
    }
    my $seq  = shift(@_);
    $seq =~ s/-+//g;                          # remove gaps ("-") from sequence
    $seq =~ s/\s+//g;                         # remove whitespace from sequence
    $seq = uc($seq);                          # convert to upper case
    if (length($seq) % 3) { # means not divisible by three
        if (substr( $seq, 0, 3 ) eq "ATG") { # start codon present, truncate from 3'
            while (length($seq) % 3) { # as long as not divisible by three, truncate from 3'
                $seq = substr( $seq, 0, -1 );
            }
        }
        elsif ((substr( $seq, -3, 3 ) eq "TAA") or (substr( $seq, -3, 3 ) eq "TGA") or (substr( $seq, -3, 3 ) eq "TAG")) { # stop codon present, truncate from 5'
            while (length($seq) % 3) { # as long as not divisible by three, truncate from 5'
                $seq = substr( $seq, 1 );
            }
        }
        else {
            $seq = (); # return emty sequence
        }
    }
    if ($seq) { # insert linebreaks if seq not empty
        $seq =~ s/(.{60})/$1\n/g
        ;    # insert linebreak after each complete block of 60 characters
        unless ( substr( $seq, -1, 1 ) eq "\n" )
        {      # add linebreak to end of sequence if not present
            $seq .= "\n";
        }
    }
    return ( $seq );
}    # end of sub mod

sub unwhite
{ # removes any whitespace (including linebreaks) from the beginning and the end of the scalar value passed to it
    unless ( scalar(@_) == 1 ) {
        die(
"\nSubroutine unwhite needs exactly one element of \@\_ (from wich any whitespace that might occur at the end will be removed), program died, please check.\n"
        );
    }
    else {
        $_[0] =~ s/^\s+//g;
        $_[0] =~ s/\s+$//g;
        return ( $_[0] );
    }
}    # end of sub unwhite